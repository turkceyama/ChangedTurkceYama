# Changed Türkçe Yama
Changed Gayri Resmi Türkçe Yama

![Banner](https://steamuserimages-a.akamaihd.net/ugc/1697278243785931593/064E8034C0F3287E9962762A80EC3F859568D997/?imw=128&imh=128&ima=fit&impolicy=Letterbox&imcolor=%23000000&letterbox=true///)

## Kurulum

### Hazırlık

Oyunun örnek dizin yolu(Windows): "C:\Program Files (x86)\Steam\steamapps\common\Changed" | Linux: "~/.local/share/Steam/steamapps/common/Changed/"

Dizin yoluna "Steam istemcisi > oyuna sağ tıklama > yerel dosyalar > göz at" şeklindeki basit adımları takip
ederek erişebilirsiniz.
 
0-Oyun dosyalarınızı yedekleyin. Oyunun ilerleme kayıtları (X.rvdata şeklinde) kendi içinde bulunuyor ve Steam Cloud desteği
şimdilik mevcut değil.

1-Bu adresten en güncel yamayı indirin: [Changed Gayri Resmi Türkçe Yama](https://gitlab.com/turkceyama/ChangedTurkceYama/-/releases).
(Örneğin: "ChangedTurkceRev20.zip")

2-İndirdiğiniz "zip" dosyasını açın ve içinde bulunan her şeyi oyunun dizini olan "Changed" klasörüne atın ve bu dizin içerisindeki "Game.rgss2a" adlı dosyayı silin.

3- Yama kurulumu tamamlandı. Yamayı etkinleştirmek için oyuna girin ve ana menüde "ENGLISH" yazan seçeneğe tıkladıktan sonra ok tuşlarıyla "TÜRKÇE" yazısını seçin. Yama hazır. Keyifli oyunlar...

(Not: Oyunun içindeki "Game.ini" dosyası varsayılan olarak tam ekran yapmaya ayarlanmıştır. İsterseniz 
"fullscreen = 1" adlı değeri "fullscreen = 0" yaparak başlangıçta tam ekrana girmesini kapatabilirsiniz.)

(Not 2: Eğer dil seçimi bölümünde "TÜRKÇE" seçeneği yerine Çince yazılar görüyorsanız yama kurulumunu tekrar yapın.) "Changed'in içerisndeki Game.rgss2a adlı dosyayı silmeyi unutmayın"
 
## İletişim

### Steam Sayfası
[Changed Türkçe Yama](https://steamcommunity.com/sharedfiles/filedetails/?id=2519890366)

## Emeği Geçenler

### DarkBloodKing
(https://steamcommunity.com/id/DarkBlood007/)


### Değiştirilen bütün assetlerin telif hakkı oyunun yapımcısı olan DragonSnow'a aittir.
### All changed assets' copyright in this game belongs to its developer DragonSnow.

### Yalnızca yerelleştirilen bölümler CC0 lisansıyla lisanslanmıştır.
### Only localised parts of the assets are licensed under CC0.
### Therefore assets themselves aren't made public domain, their license cannot be changed since they're copyrighted materials with only changed parts(like subtitles, pictures and other text) licensed under CC0 so people can do whatever they want with this translation for any purpose.

